var express = require('express')
var Main = require('./controller/main')
var mdb = require('mdb')

var router = express.Router()

router.route('/').get(Main.insertFile)
router.route('/home').get(Main.insertFile)
router.route('/toUnicode').get(Main.toUnicode)
router.route('/download/:fileName').get(Main.download)

router.route('/delete').post(Main.delete)
router.route('/mdbSql').post(Main.mdbSql)
router.route('/parseUnicodSql').post(Main.parseUnicodSql)
router.route('/upload').post(Main.upload)
router.route('/parse').post(Main.parse)



module.exports = router
